# Collaborative

## Какво е това?

Място за съвместно изготвяне на полезна информация.
Информацията трябва да е ясна, организирана тематично в документи и синтезирана. 
Документите се изготвят заедно от множество хора.

## Как да редактирам?

- https://docs.gitlab.com/ee/user/project/repository/web_editor.html
- https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html

## Какво е различното спрямо друга информация онлайн?

Всеки документ, може да се променя и еволюира.
След достигане на 3ма души които предлагат документи и промени по документи,
ще се въведе режим на общностно одобрение на всяка промяна.

Подобно на масата софтуерни проекти, всяка промяна по въведения текст
може да се инспектира кога е възникнала и от кого.

## Съдържание

- [Какво да правя докато трае карантината?](./covid-19/living_quarantine.md)
- [Храна](./covid-19/cheap_food.md)
- [Какво да правим ако не се чувстваме добре?](./covid-19/discomfort_steps.md)
- [Какво да правя с финансите по време на карантината?](./covid-19/finance_quarantine.md)
- [Хипотези и референции](./covid-19/research)

Replicated at:

https://gitlab.com/collaborative/infobg/-/tree/master/x
https://github.com/collaborativeinfo/bg
