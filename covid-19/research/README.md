## Въведение

За момента това е страница тип [Brain Storming](https://bg.wikipedia.org/wiki/%D0%9C%D0%BE%D0%B7%D1%8A%D1%87%D0%BD%D0%B0_%D0%B0%D1%82%D0%B0%D0%BA%D0%B0).
Целта е да събера повече твърдения подкрепени с поне две референции.

Интереса за сега е върху усложненията (по-скоро леталните), както и разпространението на вируса.

https://en.wikipedia.org/wiki/Coronavirus

Според записки вирусът е известен от 1960г. [^1]

През есента на 2002 година са започнали да наблюдават сериозен брой смъртни случаи в Китай,
продължили поразиите до 2003.
Въпреки високата смъртност разпространението спрямо 2020 е несравнимо малко.

Предполагам че от откриването си до сега е имал пикове и падове в разпространението си 
но те да са останали незабелязани.

### [2003 SARS-CoV](https://en.wikipedia.org/wiki/Severe_acute_respiratory_syndrome_coronavirus)

[2002–2004_SARS_outbreak](https://en.wikipedia.org/wiki/2002%E2%80%932004_SARS_outbreak)

[Bat SARS Cov Rs806/2006 spike protein gene](https://www.ncbi.nlm.nih.gov/nuccore/FJ588692.1)

[Structure of a proteolitically resistant core from the severe acute respiratory syndrome coronavirus S2 fusion protein,  Released: 22 Dec 2004 ](https://www.ebi.ac.uk/pdbe/entry/pdb/2bez)

### 2012 Middle East respiratory syndrome-related coronavirus

https://en.wikipedia.org/wiki/Middle_East_respiratory_syndrome-related_coronavirus

### [SARS-CoV-2](https://en.wikipedia.org/wiki/SARS-CoV-2)

[Severe acute respiratory syndrome coronavirus 2 isolate Wuhan-Hu-1, complete genome.](https://www.ncbi.nlm.nih.gov/nuccore/NC_045512.2)

[X-Ray Structural and Biological Evaluation of a Series of Potent and Highly Selective Inhibitors of Human Coronavirus Papain-Like Proteases](https://www.ebi.ac.uk/pdbe/entry/pdb/4ovz)

[Crystal structure (monoclinic form) of the complex resulting from the reaction between SARS-CoV-2 (2019-nCoV)](https://www.ebi.ac.uk/pdbe/entry/pdb/6y2f)


### Чернова. Проследяване на различните SARS от 2002 до сега

Bat SARS Cov Rs806/2006 spike protein gene <> SARS2 Wuhan-Hu-1, complete genome
https://www.ebi.ac.uk/Tools/services/web/toolresult.ebi?jobId=clustalo-I20200315-230920-0515-19469457-p2m

## Разпространение

### Чрез животни и лоша хигиена

Първите случаи са открити при [прилепи и камили](https://duma.bg/?go=news&p=detail&nodeId=102614).

[Допирът на Китайците до животни и секрети е умопомрачаващ.](https://www.youtube.com/watch?v=rbHxeOQA1Mc)

_Колко време здрав човек може да е преносител на вируса?_
_Може ли да се установят граници за излагане на вируса и състояние на метаболизма при които вируса няма да се прояви?_

## Хипотетични причини за летални усложнения при SARS-CoV

### 5G влияе върху разпространението на SARS-COV

### 5G влияе на развитието на SARS-CoV

Нулевата хипотеза е че 5G не влияе на развитието на вируса.

- Спомагане размножаване на вируси [^2]

- Hematopoietic and intestinal systems side effects are frequently found in patients who suffered from accidental or medical radiation exposure. [^3]


Реално все още не мога да тествам алтернативата тъй като липсва в интернет информация за конкретни честоти и интензитет на използваните мрежи, които крайните клиенти използват във Wuhan и Milano.
Липсва информация и за честотите на тестовите постановки по света които се монтират началото на 2020.

Липсват тестови анализи за ефектите от 5G

### Изследвания за влияние на 5G

[EUROPAEM EMF Guideline 2016](https://www.ncbi.nlm.nih.gov/pubmed/27454111)

http://www.defenddemocracy.press/scientists-warn-of-potential-serious-health-effects-of-5g/

### Статистика за покритие на 5G

[Taiwan bids.](https://www.digitimes.com/news/a20200116PD221.html)

[Taiwan is saved](https://www.dw.com/bg/как-/a-52752426)
Taiwan did not report a case in the statistics
https://www.who.int/docs/default-source/coronaviruse/situation-reports/20200313-sitrep-53-covid-19.pdf?sfvrsn=adb3f72_2

https://www.youtube.com/watch?v=DIV39-KOzh0

http://www.xinhuanet.com/english/2019-10/31/c_138517734.htm

https://www.vodafone5g.it/5G-a-milano.php?lang=EN

https://www.lifewire.com/5g-availability-world-4156244

## Извадка боледували и със усложнения

- Анкета за боледували в България:
  https://www.facebook.com/groups/1493230050845048/permalink/1493237274177659/

[^1]: https://tass.ru/obschestvo/2063018
[^2]: https://www.scholarsresearchlibrary.com/articles/a-brief-review-microwave-assisted-organic-reaction.pdf
[^3]: https://www.ncbi.nlm.nih.gov/pubmed/31931652
